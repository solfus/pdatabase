// Status: Not Working

// Description: This code will create a text file whose name will be choosen by the user. Next It will ask for some datas and then save it.

// Step 0: Include important headers and parameters that the program will need.
// Step 1: Declare the variables "filename", "name" and "age"
// Step 2: Ask for a filename, name and age; then initialize "filename" "name" and "age" from user input
// Step 3: Create file with the name "filename". Introduce in it "name" and "age"
// Step 4: Print a code to announce succesfull run of it

// <Program>


  // <Step 0>
#include <iostream> // This header includes Input and Output operations

using namespace std; // Let us work with cin (C Input) and cout (C Output)
// directly, istead of calling "std:: " first

  // </Step 0>


int main() { // Beginning of main program}
  // <Step 1>
  string name;
  string age;
  // </Step 1>


  // <Step 2>
  cout << "Introduce the name of the file: ";
  cin >> filename;
  cout << "\nIntroduce a name: ";
  cin >> name;
  cout << "Introduce the age: ";
  cin >> age;
  // </Step 2>

  
  // <Step 3>
  fstream inserter (pDatabase.txt, ios::out);
  inserter << "\n" << name << "\n" << age << "\n";
  // </Step 3>


  // <Step 4>
  cout << "Congratulation, Code Executed Successfuly";
  // </Step 4>

  
  // <Step 1>
  /*
    cout << "Hello World";
  */
  // </Step 1>


  // <Step 2>
  /*
    cout << "Hello World"; 
  */
  // </Step 2>

  
  return 0; // Return No Error
} ;
// </Program >
